build:
	docker build -t registry.gitlab.com/d34d-shippy/shippy-email-service .
	docker push registry.gitlab.com/d34d-shippy/shippy-email-service

run:
	docker run --net="host" \
		-p 50054 \
		-e MICRO_SERVER_ADDRESS=:50054 \
		-e MICRO_REGISTRY=mdns \
		shippy-email-service
